package com.jweb.common.file.image;

import java.awt.Color;
import java.awt.image.BufferedImage;
import java.io.ByteArrayOutputStream;
import java.util.Base64;

import javax.imageio.ImageIO;

import com.github.hui.quick.plugin.qrcode.wrapper.QrCodeGenV3;
import com.google.zxing.qrcode.decoder.ErrorCorrectionLevel;

/**
 * 
 * @author 邓超
 *
 * 2023/09/15 下午3:04:07
 */
public class QRCode {
	
	
	
	public static String generate(String content) {
		try {
			BufferedImage image = QrCodeGenV3
					.of(content)
					.setDetectColor(Color.BLUE)
					.setDetectInColor(Color.DARK_GRAY)
					.setErrorCorrection(ErrorCorrectionLevel.H)
					//.setLogo("")
					//.setLogoRate(1)
					.setPreColor(Color.RED)
					.build().asImg();
			String QRCodeBase64Image = null;
	        ByteArrayOutputStream os = new ByteArrayOutputStream();
	        try {
	            ImageIO.write(image, "png", os);
	            QRCodeBase64Image = Base64.getEncoder().encodeToString(os.toByteArray());
	        } finally {
	            os.flush();
	            os.close();
	        }

	        return "data:image/png;base64," + QRCodeBase64Image;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	public static void main(String[] args) {
		System.out.println(generate("ni 会啊"));

	}

}
